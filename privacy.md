
# About Privacy

## Permissions

### android.permission.ACCESS_NETWORK_STATE, android.permission.RECEIVE_BOOT_COMPLETED, android.permission.WAKE_LOCK

Not sure why they are here. Added by flutter.

### android.permission.READ_CALENDAR, android.permission.WRITE_CALENDAR

This app reads calendars saved on your devices, and can add calendar events to the calendar of your choice. You can deny these permissions and disable related functions, the rest of the app will still run.

### android.permission.INTERNET

This is used for ad delivery and bug tracking. These should not include any personal data. These respective websites also does NOT provide any access to your data for me.

## Companies used

Ad delivery by Admob (Google). Website: [https://policies.google.com/technologies/ads](https://policies.google.com/technologies/ads)

Bug tracking by Sentry. Website: [https://sentry.io/](https://sentry.io/)
